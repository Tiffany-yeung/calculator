import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
		
		Scanner userInput = new Scanner(System.in);
		System.out.println("Please enter your 1st number:");
		int firstInput = userInput.nextInt();
		System.out.println("Please enter +, -, * or /");
		String calcInput = userInput.next();
		System.out.println("Please enter your 2nd number:");
		int secondInput = userInput.nextInt();
		
		userInput.close();
		
		if (calcInput.equals("+")) {
			System.out.println(firstInput + secondInput);
		}
		
		if (calcInput.equals("-")) {
			System.out.println(firstInput - secondInput);
		}
		
		if (calcInput.equals("*")) {
			System.out.println(firstInput * secondInput);
		}
		
		if (calcInput.equals("/")) {
			System.out.println(firstInput / secondInput);
		}
	}
}
